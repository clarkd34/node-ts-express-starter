import express from 'express'

const app: express.Application = express();

app.get('/', (req, res) => {
    res.send('Hello World!')
});

app.listen(3000, () => {
    console.log('This app runs on typescript!');
});
